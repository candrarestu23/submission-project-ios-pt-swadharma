//
//  submissionProjectTests.swift
//  submissionProjectTests
//
//  Created by candra restu on 22/05/22.
//

import XCTest
import RxSwift
import Swinject
import RealmSwift
@testable import submissionProject

class submissionProjectTests: XCTestCase {

    var portofolioViewModel: PortofolioViewModel!
    var qrisViewModel: QrisViewModel!
    var promoViewModel: PromoViewModel!
    var container: Container!
    var realm: Realm!
    
    private func setupPortofolioController() {
        let portofolioController = PortofolioViewController(
            nibName: "PortofolioViewController",
            bundle: nil)
        
        container.register(PortofolioRouter.self) { _ -> PortofolioRouter in
            return PortofolioRouter(view: portofolioController)
        }
        
        container.register(PortofolioViewModel.self) { resolver -> PortofolioViewModel in
            let remote = MockDataSource()
            let local = MockLocalDataSource(realm: self.realm)
            let repository = MockTransactionRepository(locale: local, remote: remote)
            let interactor = PortofolioInteractor(repository: repository)
            let router = resolver.resolve(PortofolioRouter.self)
            
            return PortofolioViewModel(portofolioUseCase: interactor, router: router)
        }
    }
    
    private func setupQrisController() {
        let qrisViewController = QrisViewController(
            nibName: "QrisViewController",
            bundle: nil)
        
        container.register(PortofolioRouter.self) { _ -> PortofolioRouter in
            return PortofolioRouter(view: qrisViewController)
        }
        
        container.register(QrisViewModel.self) { resolver -> QrisViewModel in
            let remote = MockDataSource()
            let local = MockLocalDataSource(realm: self.realm)
            let repository = MockTransactionRepository(locale: local, remote: remote)
            let interactor = QrisInteractor(repository: repository)
            let router = resolver.resolve(QrisRouter.self)
            
            return QrisViewModel(qrisUseCase: interactor, router: router)
        }
    }
    
    private func setupPromoController() {
        let promoViewController = PromoViewController(
            nibName: "PromoViewController",
            bundle: nil)
        
        container.register(PromoRouter.self) { _ -> PromoRouter in
            return PromoRouter(view: promoViewController)
        }
        
        container.register(PromoViewModel.self) { resolver -> PromoViewModel in
            let remote = MockDataSource()
            let local = MockLocalDataSource(realm: self.realm)
            let repository = MockTransactionRepository(locale: local, remote: remote)
            let interactor = PromoInteractor(repository: repository)
            let router = resolver.resolve(PromoRouter.self)
            
            return PromoViewModel(promoUseCase: interactor, router: router)
        }
    }
  
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.container = Container()
        realm = try! Realm()
        setupPromoController()
        setupQrisController()
        setupPortofolioController()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.container = nil
        realm = nil
        portofolioViewModel = nil
        qrisViewModel = nil
        promoViewModel = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_portofolio_get_data_type() {
        portofolioViewModel = container.resolve(PortofolioViewModel.self)
        portofolioViewModel.getPortofolio()
        XCTAssertEqual(portofolioViewModel.portofolioData.value.first?.type, "donutChart")
        XCTAssertEqual(portofolioViewModel.portofolioData.value[1].type, "lineChart")
    }
    
    func test_portofolio_get_data_type_2() {
        portofolioViewModel = container.resolve(PortofolioViewModel.self)
        portofolioViewModel.getPortofolio()
        XCTAssertEqual(portofolioViewModel.portofolioData.value.count, 2)
        XCTAssertEqual(portofolioViewModel.portofolioData.value.first?.transaction?.count, 4)
        XCTAssertEqual(portofolioViewModel.portofolioData.value[1].month?.month?.count, 12)
        XCTAssertNil(portofolioViewModel.portofolioData.value.first?.month)
        XCTAssertNil(portofolioViewModel.portofolioData.value[1].transaction)
     }
    
    func test_portofolio_get_piechart() {
        portofolioViewModel = container.resolve(PortofolioViewModel.self)
        portofolioViewModel.getPortofolio()
        XCTAssertNotNil(portofolioViewModel.pieChartData.value)
        guard let pieChartData = portofolioViewModel.pieChartData.value else { return }
        XCTAssertTrue(pieChartData.count > 0)
    }
    
    func test_portofolio_get_linechart() {
        portofolioViewModel = container.resolve(PortofolioViewModel.self)
        portofolioViewModel.getPortofolio()
        XCTAssertNotNil(portofolioViewModel.lineChartData.value)
        guard let lieChartData = portofolioViewModel.lineChartData.value else { return }
        XCTAssertTrue(lieChartData.count > 0)
    }
    
    func test_qris_get_history() {
        qrisViewModel = container.resolve(QrisViewModel.self)
        qrisViewModel.saveQris(qris: QrisHistoryModel(bank: "BNI", transactionID: "ID12345678", merchantName: "MERCHANT MOCK TEST", amount: 50000))
        qrisViewModel.getQrisHistory()
        let data = qrisViewModel.qrisData.value.last
        XCTAssertEqual(data?.bank, "BNI")
        XCTAssertEqual(data?.transactionID, "ID12345678")
        XCTAssertEqual(data?.merchantName, "MERCHANT MOCK TEST")
        XCTAssertEqual(data?.amount, 50000)
    }
    
    func test_qris_get_history_2() {
        qrisViewModel = container.resolve(QrisViewModel.self)
        qrisViewModel.saveQris(qris: QrisHistoryModel(bank: "MANDIRI", transactionID: "ID000000", merchantName: "MERCHANT TEST", amount: 100000))
        qrisViewModel.getQrisHistory()
        let data = qrisViewModel.qrisData.value.last
        XCTAssertEqual(data?.bank, "MANDIRI")
        XCTAssertEqual(data?.transactionID, "ID000000")
        XCTAssertEqual(data?.merchantName, "MERCHANT TEST")
        XCTAssertEqual(data?.amount, 100000)
    }
    
    func test_qris_convert_qr_to_model() {
        qrisViewModel = container.resolve(QrisViewModel.self)
        let qrisData = qrisViewModel.convertQrToModel(data: "BNI.ID12345678.MERCHANT MOCK TEST.50000")
        XCTAssertEqual(qrisData.bank, "BNI")
        XCTAssertEqual(qrisData.transactionID, "ID12345678")
        XCTAssertEqual(qrisData.merchantName, "MERCHANT MOCK TEST")
        XCTAssertEqual(qrisData.amount, 50000) 
    }
    
    func test_qris_convert_qr_to_model_2() {
        qrisViewModel = container.resolve(QrisViewModel.self)
        let qrisData = qrisViewModel.convertQrToModel(data: "MANDIRI.ID000000.MERCHANT TEST.100000")
        XCTAssertEqual(qrisData.bank, "MANDIRI")
        XCTAssertEqual(qrisData.transactionID, "ID000000")
        XCTAssertEqual(qrisData.merchantName, "MERCHANT TEST")
        XCTAssertEqual(qrisData.amount, 100000)
    }
    
    func test_promo_get_promo() {
        promoViewModel = container.resolve(PromoViewModel.self)
        promoViewModel.getPromo()
        XCTAssertTrue(promoViewModel.promoData.value.count > 0)
        XCTAssertEqual(promoViewModel.promoData.value.first?.name, "BNI Mobile Banking")
        XCTAssertEqual(promoViewModel.promoData.value.first?.imagesURL, "https://www.bni.co.id/Portals/1/BNI/Beranda/Images/Beranda-MobileBanking-01-M-Banking1.png")
        XCTAssertEqual(promoViewModel.promoData.value.first?.detail, "https://www.bni.co.id/id-id/individu/simulasi/bni-deposito")

        XCTAssertEqual(promoViewModel.promoData.value[1].name, "BNI Wholesale")
        XCTAssertEqual(promoViewModel.promoData.value[1].imagesURL, "https://bit.ly/MarcommBNIFleksi-2023")
        XCTAssertEqual(promoViewModel.promoData.value[1].detail, "https://www.bni.co.id/id-id/korporasi/solusi-wholesale/tentang-kami")
    }
}
