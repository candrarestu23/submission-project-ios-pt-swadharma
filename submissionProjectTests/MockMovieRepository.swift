//
//  MockMovieRepository.swift
//  submissionProjectTests
//
//  Created by candra restu on 22/05/22.
//

import Foundation
import RxSwift
@testable import submissionProject

final class MockTransactionRepository: NSObject {
    
    typealias MovieInstance = (MockLocalDataSource, MockDataSource) -> MockTransactionRepository
    
    fileprivate let remote: MockDataSource
    fileprivate let locale: MockLocalDataSource
    
    init(locale: MockLocalDataSource, remote: MockDataSource) {
        self.locale = locale
        self.remote = remote
    }
    
    static let sharedInstance: MovieInstance = { localeRepo, remoteRepo in
        return MockTransactionRepository(locale: localeRepo, remote: remoteRepo)
    }
    
}

extension MockTransactionRepository: TransactionRepositoryDelegate {
    func getPortofolio() -> RxSwift.Observable<PortofolioModel> {
        return self.remote.getPortofolio()
            .map {
                return TransactionMapper.mapPortofolioToModel(input: $0)
            }
    }
    
    func getPromo() -> RxSwift.Observable<PromoModel> {
        return self.remote.getPromo()
            .map {
                return TransactionMapper.mapPromoResponseToModel(input: $0)
            }
    }
    
    func getQrisHistory() -> RxSwift.Observable<[QrisHistoryModel]> {
        self.locale.getQrisHistory()
            .map {
                return TransactionMapper.mapQrisEntityToModel(input: $0)
            }
    }
    
    func saveQris(qris: submissionProject.QrisHistoryModel) -> Observable<Bool> {
        self.locale.addQris(from: TransactionMapper.mapQrisModelToEntity(input: qris))

    }
}
