//
//  MockGameDataSource.swift
//  submissionProjectTests
//
//  Created by candra restu on 22/05/22.
//

import Foundation
import RxSwift
import RxCocoa
@testable import submissionProject

final class MockDataSource {
    
    public typealias Request = URLRequest
    public typealias Response = MovieListResponse
    public init() {}
    
    public func getPortofolio() -> Observable<PortofolioResponse> {
        return Observable<PortofolioResponse>.create { observer in
            let decoder = JSONDecoder()
            
            guard let pathString = Bundle(for: type(of: self))
                .path(forResource: "MockPortofolio", ofType: "json") else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            let data = jsonString.data(using: .utf8)!
            
            do {
                let test = try decoder.decode(PortofolioResponse.self, from: data)
                observer.onNext(test)
            } catch let error {
                observer.onError(error)
            }
            
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    public func getPromo() -> Observable<PromoResponse> {
        return Observable<PromoResponse>.create { observer in
            let decoder = JSONDecoder()
            
            guard let pathString = Bundle(for: type(of: self))
                .path(forResource: "MockPromo", ofType: "json") else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            let data = jsonString.data(using: .utf8)!
            
            do {
                let test = try decoder.decode(PromoResponse.self, from: data)
                observer.onNext(test)
            } catch let error {
                observer.onError(error)
            }
            
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
