//
//  MockLocalDataSource.swift
//  submissionProjectTests
//
//  Created by candra restu on 22/05/22.
//

import Foundation
import Foundation
import RxSwift
import RealmSwift
@testable import submissionProject

final class MockLocalDataSource {
    
    public typealias Response = QrisEntity
    public typealias Request = QrisEntity
    private let _realm: Realm?
    
    public init(realm: Realm) {
        _realm = realm
    }
    
    func getQrisHistory() -> RxSwift.Observable<[QrisEntity]> {
        return Observable<[QrisEntity]>.create { observer in
            if let realm = self._realm {
                let categories: Results<QrisEntity> = {
                    realm.objects(QrisEntity.self)
                        .sorted(byKeyPath: "bank", ascending: true)
                }()
                observer.onNext(categories.toArray(ofType: QrisEntity.self))
                observer.onCompleted()
            } else {
                observer.onError(APIError.localDatabaseError)
            }
            return Disposables.create()
        }
    }
    
    func addQris(from movie: QrisEntity) -> RxSwift.Observable<Bool> {
        return Observable<Bool>.create { observer in
            if let realm = self._realm {
                do {
                    try realm.write {
                        realm.add(movie, update: .all)
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                } catch {
                    observer.onError(APIError.localDatabaseError)
                }
            } else {
                observer.onError(APIError.localDatabaseError)
            }
            return Disposables.create()
        }
    }

}
