//
//  QrisViewModel.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import RxSwift
import RxCocoa

class QrisViewModel: ObservableObject {
    
    private let disposeBag = DisposeBag()
    private var qrisUseCase: QrisUseCase?
    private var router: QrisRouter?
    
    let isLoading = ObservableData<Bool>()
    let isEmpty = ObservableData<Bool>()
    let errorMessage = ObservableData<String>()
    let qrisData = BehaviorRelay<[QrisHistoryModel]>(value: [])
    
    init(qrisUseCase: QrisUseCase?, router: QrisRouter?) {
        self.qrisUseCase = qrisUseCase
        self.router = router
    }
    
    func getQrisHistory() {
        qrisUseCase?.getQrisHistory()
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] result in
                self?.qrisData.accept(result)
                self?.isEmpty.value = result.isEmpty
            } onError: { [weak self] error in
                self?.errorMessage.value = error.localizedDescription
                self?.isEmpty.value = true
            } onCompleted: {
                self.isLoading.value = false
            }.disposed(by: disposeBag)
    }
    
    func saveQris(qris: QrisHistoryModel) {
        qrisUseCase?.addQrisHistory(qris)
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] result in
                if result {
                    self?.routeToDetail(data: qris)
                }
            } onError: { [weak self] error in
                self?.errorMessage.value = error.localizedDescription
            } onCompleted: {
                self.isLoading.value = false
            }.disposed(by: disposeBag)
    }
    
    func routeToScan() {
        router?.routeToScanner(didSuccess: { [weak self] qrString in
            guard let data = self?.convertQrToModel(data: qrString) else { return }
            self?.saveQris(qris: data)
        })
    }
    
    func convertQrToModel(data: String) -> QrisHistoryModel {
        let component = data.components(separatedBy: ".")
        return QrisHistoryModel(
            bank: component[0],
            transactionID: component[1],
            merchantName: component[2],
            amount: Int(component[3]))
    }
    
    func routeToDetail(data: QrisHistoryModel) {
        router?.routeToDetail(data: data)
    }
}
