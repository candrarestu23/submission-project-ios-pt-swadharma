//
//  QrisHistoryItemTableViewCell.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit

class QrisHistoryItemTableViewCell: UITableViewCell {

    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var transactionIDLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setupCell(data: QrisHistoryModel) {
        bankLabel.text = data.bank ?? ""
        transactionIDLabel.text = data.transactionID ?? ""
        merchantNameLabel.text = data.merchantName ?? ""
        amountLabel.text = data.amount?.convertToCurrency() ?? "Rp0"
    }
}
