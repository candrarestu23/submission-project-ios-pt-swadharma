//
//  QrisViewController.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit
import RxSwift
import RxCocoa

class QrisViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    var viewModel: QrisViewModel?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindData()
        viewModel?.getQrisHistory()
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getQrisHistory()
    }
    
    private func setupTableView() {
        let nib = UINib(nibName: "QrisHistoryItemTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "QrisHistoryItemTableViewCell")
    }
    
    private func bindData() {
        viewModel?.qrisData
            .bind(to:tableView.rx.items(
                cellIdentifier: "QrisHistoryItemTableViewCell",
                cellType: QrisHistoryItemTableViewCell.self)) { [weak self] index, item, cell in
                    cell.setupCell(data: item)
                }.disposed(by: disposeBag)
        
        viewModel?.isEmpty.observe(disposeBag) { [weak self] isEmpty in
            self?.noDataLabel.isHidden = !(isEmpty ?? false)
        }
    }

    @IBAction func onClickScan(_ sender: Any) {
        viewModel?.routeToScan()
    }
}
