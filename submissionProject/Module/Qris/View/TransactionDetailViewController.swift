//
//  TransactionDetailViewController.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit

class TransactionDetailViewController: UIViewController {

    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var transactionIDLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var data: QrisHistoryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bankLabel.text = data?.bank ?? ""
        transactionIDLabel.text = data?.transactionID ?? ""
        merchantNameLabel.text = data?.merchantName ?? ""
        amountLabel.text = data?.amount?.convertToCurrency()
    }

}
