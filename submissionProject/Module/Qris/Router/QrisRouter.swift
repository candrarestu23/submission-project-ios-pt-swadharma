//
//  QrisRouter.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import UIKit

protocol QrisRoutingDelegate : AnyObject {
    func routeToScanner(didSuccess: ((String) -> Void)?)
    func routeToDetail(data: QrisHistoryModel)
}

class QrisRouter {

    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension QrisRouter: QrisRoutingDelegate {
    func routeToDetail(data: QrisHistoryModel) {
        let viewController = QrisBuilder.buildDetail(data: data)
        self.view.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func routeToScanner(didSuccess:  ((String) -> Void)?) {
        let viewController = QrisBuilder.buildScan(didSuccessScan: didSuccess)
        self.view.present(viewController, animated: true)
    }
}
