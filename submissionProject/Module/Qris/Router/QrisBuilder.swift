//
//  QrisBuilder.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import UIKit
import Swinject
import SwinjectStoryboard

class QrisBuilder {
    static func build(
        usingNavigationFactory factory: NavigationFactory,
        container: Container
    ) -> UIViewController {
        Injection.init().provideQris(container: container)
        return factory(container.resolve(QrisViewController.self) ?? UIViewController())
    }
    
    static func buildScan(
        didSuccessScan: ((String) -> Void)?
    ) -> UIViewController {
        let viewController = ScanQrisViewController()
        viewController.didSuccessScan = didSuccessScan
        return viewController
    }
    
    static func buildDetail(
        data: QrisHistoryModel
    ) -> UIViewController {
        let viewController = TransactionDetailViewController()
        viewController.data = data
        return viewController
    }
}
