//
//  PortofolioBuilder.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation
import UIKit
import Swinject
import SwinjectStoryboard

class PortofolioBuilder {
    static func build(
        usingNavigationFactory factory: NavigationFactory,
        container: Container
    ) -> UIViewController {
        Injection.init().providePortofolio(container: container)
        return factory(container.resolve(PortofolioViewController.self) ?? UIViewController())
    }
    
    static func buildDetail(
        data: TransactionTypeModel
    ) -> UIViewController {
        let viewController = PortofolioDetailViewController()
        viewController.data = data
        return viewController
    }
}
