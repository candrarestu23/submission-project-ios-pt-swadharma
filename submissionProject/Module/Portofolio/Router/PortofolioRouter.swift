//
//  PortofolioRouter.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import UIKit

protocol PortofolioRoutingDelegate : AnyObject {
    func routeToDetailPage(_ data: TransactionTypeModel)
}

class PortofolioRouter {

    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension PortofolioRouter: PortofolioRoutingDelegate {
    func routeToDetailPage(_ data: TransactionTypeModel) {
        let detailViewController = PortofolioBuilder.buildDetail(data: data)
        self.view.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
