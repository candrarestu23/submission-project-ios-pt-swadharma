//
//  PortofolioViewModel.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation
import RxSwift
import RxCocoa
import DGCharts

class PortofolioViewModel: ObservableObject {
    
    private let disposeBag = DisposeBag()
    private var portofolioUseCase: PortofolioUseCase?
    private var router: PortofolioRouter?
    
    let isLoading = ObservableData<Bool>()
    let isEmpty = ObservableData<Bool>()
    let errorMessage = ObservableData<String>()
    let portofolioData = BehaviorRelay<PortofolioModel>(value: [])
    var pieChartData = ObservableData<PieChartData>()
    var lineChartData = ObservableData<LineChartData>()
    
    init(portofolioUseCase: PortofolioUseCase?, router: PortofolioRouter?) {
        self.portofolioUseCase = portofolioUseCase
        self.router = router
    }
    
    func getPortofolio() {
        portofolioUseCase?.getPortofolio()
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] result in
                self?.portofolioData.accept(result)
                self?.isEmpty.value = result.isEmpty
                self?.setupPieChartData()
                self?.setupLineChartData()
            } onError: { [weak self] error in
                self?.errorMessage.value = error.localizedDescription
            } onCompleted: {
                self.isLoading.value = false
            }.disposed(by: disposeBag)
    }
    
    func setupPieChartData() {
        var entries = [ChartDataEntry]()
        
        for item in portofolioData.value.first?.transaction ?? [] {
            entries.append(ChartDataEntry(
                x: Double(item.percentage ?? "0") ?? 0,
                y: Double(item.percentage ?? "0") ?? 0))
        }
        
        let set = PieChartDataSet(entries: entries)
        set.colors = ChartColorTemplates.colorful()
        let data = PieChartData(dataSet: set)
        pieChartData.value = data
    }
    
    func setupLineChartData() {
        var entries = [ChartDataEntry]()
        var index = 0
        for item in portofolioData.value.at(index: 1)?.month?.month ?? [] {
            entries.append(ChartDataEntry(
                x: Double(index),
                y: Double(item)))
            index += 1
        }
        
        let set = LineChartDataSet(entries: entries)
        set.colors = ChartColorTemplates.material()
        let data = LineChartData(dataSet: set)
        lineChartData.value = data
    }
    
    func routeToDetail(value: String) {
        guard let detailData = portofolioData.value.first?.transaction?.filter({
            $0.percentage == value
        }).first else { return }
        router?.routeToDetailPage(detailData)
    }
}
