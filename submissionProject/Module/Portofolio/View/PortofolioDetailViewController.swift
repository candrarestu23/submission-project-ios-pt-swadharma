//
//  PortofolioDetailViewController.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit

class PortofolioDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var data: TransactionTypeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = data?.label
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.rowHeight = 80
    }
}

extension PortofolioDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let data = data?.data?.at(index: indexPath.row)
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.text = "Date: \(data?.trxDate ?? "")\nAmount:\(data?.nominal?.convertToCurrency() ?? "Rp 0")"
        return cell
    }
}
