//
//  PortofolioViewController.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import UIKit
import RxSwift
import RxCocoa
import DGCharts

class PortofolioViewController: UIViewController {

    @IBOutlet weak var pieChartContainerView: UIView!
    @IBOutlet weak var lineChartContainerView: UIView!
    
    var viewModel: PortofolioViewModel?
    var disposeBag = DisposeBag()
    var pieChart = PieChartView()
    var lineChart = LineChartView()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObserver()
        viewModel?.getPortofolio()
    }
    
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.setupViews()
        }
    }
    
    private func setupViews() {
        self.title = "Portofolio"
        navigationController?.navigationBar.prefersLargeTitles = false
        pieChart.frame = pieChartContainerView.frame
        pieChart.rotationEnabled = false
        pieChart.delegate = self
        pieChartContainerView.addSubview(pieChart)
        
        lineChart.frame = CGRect(
            x: 0,
            y: 0,
            width: lineChartContainerView.frame.width,
            height: lineChartContainerView.frame.height)
        lineChart.delegate = self
        lineChartContainerView.addSubview(lineChart)
    }
    
    private func setupObserver() {
        viewModel?.pieChartData.observe(disposeBag) { [weak self] pieChartData in
            self?.pieChart.data = pieChartData
        }
        
        viewModel?.lineChartData.observe(disposeBag) { [weak self] lineChartData in
            self?.lineChart.data = lineChartData
        }
    }
}

extension PortofolioViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        viewModel?.routeToDetail(value: entry.x.stringValueRemovingTrailingZero())
    }
}
