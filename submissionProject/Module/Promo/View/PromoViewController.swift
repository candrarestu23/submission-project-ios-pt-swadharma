//
//  PromoViewController.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit
import RxCocoa
import RxSwift

class PromoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var viewModel: PromoViewModel?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindData()
        viewModel?.getPromo()
    }
    
    private func setupTableView() {
        self.title = "Promo"
        navigationController?.navigationBar.prefersLargeTitles = false
        let nibName = UINib(nibName: "PromoItemTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "PromoItemTableViewCell")
    }
    
    private func bindData() {
        viewModel?.promoData
            .bind(to:tableView.rx.items(
                cellIdentifier: "PromoItemTableViewCell",
                cellType: PromoItemTableViewCell.self)) { [weak self] index, item, cell in
                    cell.setupCell(data: item.name ?? "")
                }.disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.viewModel?.routeToDetail(index: indexPath.row)
            }).disposed(by: disposeBag)
    }
}
