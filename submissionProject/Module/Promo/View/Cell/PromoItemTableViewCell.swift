//
//  PromoItemTableViewCell.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit

class PromoItemTableViewCell: UITableViewCell {

    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var promoName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(data: String) {
        promoImage.image = UIImage(named: "dummy-image")
        promoName.text = data
    }
}
