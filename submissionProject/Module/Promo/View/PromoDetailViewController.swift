//
//  PromoDetailViewController.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import UIKit
import WebKit

class PromoDetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var data: PromoListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    private func setupViews() {
        navigationController?.navigationBar.prefersLargeTitles = false
        if let url = URL(string: data?.detail ?? "") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
}
