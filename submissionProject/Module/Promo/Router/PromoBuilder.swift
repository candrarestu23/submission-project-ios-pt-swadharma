//
//  PromoBuilder.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import UIKit
import Swinject
import SwinjectStoryboard

class PromoBuilder {
    static func build(
        usingNavigationFactory factory: NavigationFactory,
        container: Container
    ) -> UIViewController {
        Injection.init().providePromo(container: container)
        return factory(container.resolve(PromoViewController.self) ?? UIViewController())
    }
    
    static func buildDetail(
        data: PromoListModel
    ) -> UIViewController {
        let viewController = PromoDetailViewController()
        viewController.data = data
        return viewController
    }
}
