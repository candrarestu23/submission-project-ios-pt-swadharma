//
//  PromoRouter.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import UIKit

protocol PromoRoutingDelegate : AnyObject {
    func routeToDetailPage(_ data: PromoListModel)
}

class PromoRouter {

    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension PromoRouter: PromoRoutingDelegate {
    func routeToDetailPage(_ data: PromoListModel) {
        let viewController = PromoBuilder.buildDetail(data: data)
        self.view.navigationController?.pushViewController(viewController, animated: true)
    }
}
