//
//  PromoViewModel.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import RxSwift
import RxCocoa

class PromoViewModel: ObservableObject {
    
    private let disposeBag = DisposeBag()
    private var promoUseCase: PromoUseCase?
    private var router: PromoRouter?
    
    let isLoading = ObservableData<Bool>()
    let isEmpty = ObservableData<Bool>()
    let errorMessage = ObservableData<String>()
    let promoData = BehaviorRelay<[PromoListModel]>(value: [])
    
    init(promoUseCase: PromoUseCase?, router: PromoRouter?) {
        self.promoUseCase = promoUseCase
        self.router = router
    }
    
    func getPromo() {
        promoUseCase?.getPromo()
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] result in
                self?.promoData.accept(result.promos ?? [])
            } onError: { [weak self] error in
                self?.errorMessage.value = error.localizedDescription
            } onCompleted: {
                self.isLoading.value = false
            }.disposed(by: disposeBag)
    }
    
    func routeToDetail(index: Int) {
        guard let data = promoData.value.at(index: index) else { return }
        router?.routeToDetailPage(data)
    }
}
