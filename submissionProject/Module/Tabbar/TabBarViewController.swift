//
//  TabBarViewController.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import UIKit

typealias Tabs = (
    portofolio: UIViewController,
    qris: UIViewController,
    promo: UIViewController
)

class TabBarViewController: UITabBarController {
    
    init(tabs: Tabs) {
        super.init(nibName: nil, bundle: nil)
        viewControllers = [tabs.portofolio, tabs.qris, tabs.promo]
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
