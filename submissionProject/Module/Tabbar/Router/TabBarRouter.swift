//
//  TabBarRouter.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation
import UIKit

class TabBarRouter {
    
    var viewController: UIViewController
    
    typealias SubModules = (
        portofolio: UIViewController,
        qris: UIViewController,
        promo: UIViewController
    )
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension TabBarRouter {
    
    static func tabs(usingSubmodules submodules: SubModules) -> Tabs {
        let portofolioTabBarItem = UITabBarItem(title: "Portofolio", image: UIImage(systemName: "chart.bar.xaxis.ascending"), tag: 1)
        let qrisTabBarItem = UITabBarItem(title: "Qris", image: UIImage(systemName: "qrcode.viewfinder"), tag: 2)
        let promoTabBarItem = UITabBarItem(title: "Promo", image: UIImage(systemName: "bell.fill"), tag: 3)
        
        submodules.portofolio.tabBarItem = portofolioTabBarItem
        submodules.qris.tabBarItem = qrisTabBarItem
        submodules.promo.tabBarItem = promoTabBarItem
        
        return (
            portofolio: submodules.portofolio,
            qris: submodules.qris,
            promo: submodules.promo
        )
    }
}
