//
//  Injection.swift
//  submissionProject
//
//  Created by candra restu on 19/05/22.
//

import Foundation
import RealmSwift
import Swinject
import SwinjectStoryboard

final class Injection: NSObject {
    
    private func provideRepository() -> TransactionRepositoryDelegate {
        let realm = try? Realm()
        
        let locale: LocaleDataSource = LocaleDataSource.sharedInstance(realm)
        let remote: RemoteDataSource = RemoteDataSource.sharedInstance
        
        return TransactionRepository.sharedInstance(locale, remote)
    }
  
    func providePortofolio() -> PortofolioUseCase {
        let repository = provideRepository()
        return PortofolioInteractor(repository: repository)
    }
    
    func providePromo() -> PromoUseCase {
        let repository = provideRepository()
        return PromoInteractor(repository: repository)
    }
    
    func provideQris() -> QrisUseCase {
        let repository = provideRepository()
        return QrisInteractor(repository: repository)
    }
    
    
    func providePortofolio(container: Container) {
        let portofolioViewController = PortofolioViewController(
            nibName: "PortofolioViewController",
            bundle: nil)
        
        container.register(PortofolioRouter.self) { _ -> PortofolioRouter in
            return PortofolioRouter(view: portofolioViewController)
        }
        
        container.register(PortofolioViewModel.self) { resolver -> PortofolioViewModel in
            return PortofolioViewModel(portofolioUseCase: self.providePortofolio(),
                                       router: resolver.resolve(PortofolioRouter.self)!)
        }
        
        container.register(PortofolioViewController.self) { resolver -> PortofolioViewController in
            portofolioViewController.viewModel = resolver.resolve(PortofolioViewModel.self)
            return portofolioViewController
        }
    }
    
    func providePromo(container: Container) {
        let promoViewController = PromoViewController(
            nibName: "PromoViewController",
            bundle: nil)
        
        container.register(PromoRouter.self) { _ -> PromoRouter in
            return PromoRouter(view: promoViewController)
        }
        
        container.register(PromoViewModel.self) { resolver -> PromoViewModel in
            return PromoViewModel(promoUseCase: self.providePromo(),
                                  router: resolver.resolve(PromoRouter.self)!)
        }
        
        container.register(PromoViewController.self) { resolver -> PromoViewController in
            promoViewController.viewModel = resolver.resolve(PromoViewModel.self)
            return promoViewController
        }
    }
    
    func provideQris(container: Container) {
        let qrisViewController = QrisViewController(
            nibName: "QrisViewController",
            bundle: nil)
        
        container.register(QrisRouter.self) { _ -> QrisRouter in
            return QrisRouter(view: qrisViewController)
        }
        
        container.register(QrisViewModel.self) { resolver -> QrisViewModel in
            return QrisViewModel(qrisUseCase: self.provideQris(),
                                  router: resolver.resolve(QrisRouter.self)!)
        }
        
        container.register(QrisViewController.self) { resolver -> QrisViewController in
            qrisViewController.viewModel = resolver.resolve(QrisViewModel.self)
            return qrisViewController
        }
    }
}
