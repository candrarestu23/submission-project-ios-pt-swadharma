//
//  Int+Ext.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
extension Int {
    func convertToCurrency() -> String? {
         let formatter = NumberFormatter()
         formatter.numberStyle = .currency
         return formatter.string(from: NSNumber(value: self))
     }
}
