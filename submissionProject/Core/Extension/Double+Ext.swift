//
//  Double+Ext.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation

extension Double {
    func stringValueRemovingTrailingZero() -> String {
        return truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
