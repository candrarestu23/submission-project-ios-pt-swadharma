//
//  PortofolioInteractor.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation
import RxSwift

protocol PortofolioUseCase {
    func getPortofolio() -> Observable<PortofolioModel>
}

class PortofolioInteractor: PortofolioUseCase {
    private let repository: TransactionRepositoryDelegate
    
    required init(repository: TransactionRepositoryDelegate) {
        self.repository = repository
    }
    
    func getPortofolio() -> RxSwift.Observable<PortofolioModel> {
        return repository.getPortofolio()
    }
}
