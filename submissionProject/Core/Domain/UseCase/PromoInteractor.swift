//
//  PromoInteractor.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import RxSwift

protocol PromoUseCase {
    func getPromo() -> Observable<PromoModel>
}

class PromoInteractor: PromoUseCase {
    private let repository: TransactionRepositoryDelegate
    
    required init(repository: TransactionRepositoryDelegate) {
        self.repository = repository
    }
    
    func getPortofolio() -> RxSwift.Observable<PortofolioModel> {
        return repository.getPortofolio()
    }
    
    func getPromo() -> RxSwift.Observable<PromoModel> {
        return repository.getPromo()
    }
}
