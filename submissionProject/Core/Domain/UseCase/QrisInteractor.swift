//
//  QrisUseCase.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import Foundation
import RxSwift

protocol QrisUseCase {
    func getQrisHistory() -> Observable<[QrisHistoryModel]>
    func addQrisHistory(_ qris: QrisHistoryModel) -> Observable<Bool>
}

class QrisInteractor: QrisUseCase {

    private let repository: TransactionRepositoryDelegate
    
    required init(repository: TransactionRepositoryDelegate) {
        self.repository = repository
    }
    
    func getQrisHistory() -> RxSwift.Observable<[QrisHistoryModel]> {
        repository.getQrisHistory()
    }
    
    func addQrisHistory(_ qris: QrisHistoryModel) -> RxSwift.Observable<Bool> {
        repository.saveQris(qris: qris)
    }
}
