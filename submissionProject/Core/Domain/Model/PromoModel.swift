//
//  PromoModel.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation

struct PromoModel {
    var promos: [PromoListModel]?
}

struct PromoListModel {
    var id: Int?
    var name: String?
    var imagesURL: String?
    var detail: String?
}
