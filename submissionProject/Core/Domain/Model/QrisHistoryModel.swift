//
//  QrisHistoryModel.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation

struct QrisHistoryModel {
    var bank: String?
    var transactionID: String?
    var merchantName: String?
    var amount: Int?
}
