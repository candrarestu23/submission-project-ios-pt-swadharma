//
//  PortofolioModel.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation

struct PortofolioModelElement {
    var type: String?
    var transaction: [TransactionTypeModel]?
    var month: MonthModel?
}

struct TransactionTypeModel {
    var label, percentage: String?
    var data: [TransactionDataModel]?
}

struct TransactionDataModel {
    var trxDate: String?
    var nominal: Int?
}

struct MonthModel {
    var month: [Int]?
}

typealias PortofolioModel = [PortofolioModelElement]
