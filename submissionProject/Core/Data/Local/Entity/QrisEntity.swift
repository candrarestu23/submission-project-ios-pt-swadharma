//
//  QrisEntity.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
import RealmSwift

class QrisEntity: Object {
    
    @objc dynamic var bank: String = ""
    @objc dynamic var transactionID: String = ""
    @objc dynamic var merchantName: String = ""
    @objc dynamic var amount: Int = 0
    @objc dynamic var timeStamp: String = ""
    
    override static func primaryKey() -> String? {
        return "timeStamp"
    }
}
