//
//  LocalDataSource.swift
//  submissionProject
//
//  Created by candra restu on 19/05/22.
//

import Foundation
import RealmSwift
import RxSwift

protocol LocaleDataSourceProtocol: AnyObject {
    func getQrisHistory() -> Observable<[QrisEntity]>
    func addQris(from movie: QrisEntity) -> Observable<Bool>
}

final class LocaleDataSource: NSObject {
    
    private let realm: Realm?
    
    private init(realm: Realm?) {
        self.realm = realm
    }
    
    static let sharedInstance: (Realm?) -> LocaleDataSource = { realmDatabase in
        return LocaleDataSource(realm: realmDatabase)
    }
    
}

extension LocaleDataSource: LocaleDataSourceProtocol {
    func getQrisHistory() -> RxSwift.Observable<[QrisEntity]> {
        return Observable<[QrisEntity]>.create { observer in
            if let realm = self.realm {
                let categories: Results<QrisEntity> = {
                    realm.objects(QrisEntity.self)
                        .sorted(byKeyPath: "bank", ascending: true)
                }()
                observer.onNext(categories.toArray(ofType: QrisEntity.self))
                observer.onCompleted()
            } else {
                observer.onError(APIError.localDatabaseError)
            }
            return Disposables.create()
        }
    }
    
    func addQris(from movie: QrisEntity) -> RxSwift.Observable<Bool> {
        return Observable<Bool>.create { observer in
            if let realm = self.realm {
                do {
                    try realm.write {
                        realm.add(movie, update: .all)
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                } catch {
                    observer.onError(APIError.localDatabaseError)
                }
            } else {
                observer.onError(APIError.localDatabaseError)
            }
            return Disposables.create()
        }
    }
}

extension Results {
    
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for index in 0 ..< count {
            if let result = self[index] as? T {
                array.append(result)
            }
        }
        return array
    }
    
}
