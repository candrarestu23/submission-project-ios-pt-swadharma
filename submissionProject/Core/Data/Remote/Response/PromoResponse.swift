//
//  PromoResponse.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation
struct PromoResponse: Codable {
    var promos: [PromoListResponse]?
}

struct PromoListResponse: Codable {
    var id: Int?
    var name: String?
    var imagesURL: String?
    var detail: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case imagesURL = "images_url"
        case detail
    }
}
