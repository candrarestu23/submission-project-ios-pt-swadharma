//
//  PortofolioResponse.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation

enum DataUnion: Codable {
    case dataClass(MonthResponse)
    case dataDatumArray([TransactionTypeResponse])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let transactions = try? container.decode([TransactionTypeResponse].self) {
            self = .dataDatumArray(transactions)
            return
        }
        if let month = try? container.decode(MonthResponse.self) {
            self = .dataClass(month)
            return
        }
        throw DecodingError.typeMismatch(DataUnion.self,
                                         DecodingError.Context(
                                            codingPath: decoder.codingPath,
                                            debugDescription: "Wrong type for DataUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .dataClass(let month):
            try container.encode(month)
        case .dataDatumArray(let transaction):
            try container.encode(transaction)
        }
    }
}

struct PortofolioResponseElement: Codable {
    var type: String?
    var data: DataUnion?
}

struct TransactionTypeResponse: Codable {
    var label, percentage: String?
    var data: [TransactionDataResponse]?
}

struct TransactionDataResponse: Codable {
    var trxDate: String?
    var nominal: Int?

    enum CodingKeys: String, CodingKey {
        case trxDate = "trx_date"
        case nominal
    }
}

struct MonthResponse: Codable {
    var month: [Int]?
}

typealias PortofolioResponse = [PortofolioResponseElement]
