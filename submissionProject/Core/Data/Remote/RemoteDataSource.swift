//
//  RemoteDataSource.swift
//  submissionProject
//
//  Created by candra restu on 19/05/22.
//

import Foundation
import Alamofire
import RxSwift

protocol RemoteDataSourceProtocol: AnyObject {
    func getPortofolio() -> Observable<PortofolioResponse>
    func getPromo(from endpoint: TransactionAPI) -> Observable<PromoResponse>
}

final class RemoteDataSource: NSObject {
    
    private override init() { }
    
    static let sharedInstance: RemoteDataSource =  RemoteDataSource()
    
}

extension RemoteDataSource: RemoteDataSourceProtocol {
    func getPortofolio() -> RxSwift.Observable<PortofolioResponse> {
        return Observable<PortofolioResponse>.create { observer in
            let decoder = JSONDecoder()
            
            guard let pathString = Bundle(for: type(of: self))
                .path(forResource: "MockPortofolio", ofType: "json") else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
                observer.onError(fatalError())
                return Disposables.create()
            }
            
            let data = jsonString.data(using: .utf8)!
            
            do {
                let test = try decoder.decode(PortofolioResponse.self, from: data)
                observer.onNext(test)
            } catch let error {
                observer.onError(error)
            }
            
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func getPromo(from endpoint: TransactionAPI) -> RxSwift.Observable<PromoResponse> {
        return Observable<PromoResponse>.create { observer in
            AF.request(endpoint.urlRequest)
                .validate()
                .responseDecodable(of: PromoResponse.self) { response in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
            return Disposables.create()
        }
    }
    
}
