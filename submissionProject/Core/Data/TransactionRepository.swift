//
//  TransactionRepository.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation
import RxSwift

protocol TransactionRepositoryDelegate: AnyObject {
    func getPortofolio() -> Observable<PortofolioModel>
    func getPromo() -> Observable<PromoModel>
    func getQrisHistory() -> Observable<[QrisHistoryModel]>
    func saveQris(qris: QrisHistoryModel) -> Observable<Bool>
}

final class TransactionRepository: NSObject {
    
    typealias TransactionInstance = (LocaleDataSource, RemoteDataSource) -> TransactionRepository
    
    fileprivate let remote: RemoteDataSource
    fileprivate let locale: LocaleDataSource
    
    private init(locale: LocaleDataSource, remote: RemoteDataSource) {
        self.locale = locale
        self.remote = remote
    }
    
    static let sharedInstance: TransactionInstance = { localeRepo, remoteRepo in
        return TransactionRepository(locale: localeRepo, remote: remoteRepo)
    }
}

extension TransactionRepository: TransactionRepositoryDelegate {
    func getQrisHistory() -> RxSwift.Observable<[QrisHistoryModel]> {
        self.locale.getQrisHistory()
            .map {
                return TransactionMapper.mapQrisEntityToModel(input: $0)
            }
    }
    
    func saveQris(qris: QrisHistoryModel) -> RxSwift.Observable<Bool> {
        self.locale.addQris(from: TransactionMapper.mapQrisModelToEntity(input: qris))
    }
    
    func getPortofolio() -> RxSwift.Observable<PortofolioModel> {
        return self.remote.getPortofolio()
            .map {
                return TransactionMapper.mapPortofolioToModel(input: $0)
            }
    }
    
    func getPromo() -> RxSwift.Observable<PromoModel> {
        return self.remote.getPromo(from: .getPromo)
            .map {
                return TransactionMapper.mapPromoResponseToModel(input: $0)
            }
    }
    
}
