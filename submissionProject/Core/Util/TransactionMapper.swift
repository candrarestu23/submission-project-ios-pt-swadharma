//
//  TransactionMapper.swift
//  submissionProject
//
//  Created by candra restu on 04/03/24.
//

import Foundation

final class TransactionMapper {
    static func mapPortofolioToModel(
        input portofolioResponse: PortofolioResponse
    ) -> PortofolioModel {
        var portofolioModel: PortofolioModel = []
        
        for portofolioElement in portofolioResponse {
            let type = portofolioElement.type
            let dataUnion = portofolioElement.data
            
            var transactionTypeModels: [TransactionTypeModel] = []
            var monthModel: MonthModel?
            
            switch dataUnion {
            case .dataClass(let monthResponse):
                if let month = monthResponse.month {
                    monthModel = MonthModel(month: month)
                }
            case .dataDatumArray(let transactionTypeResponses):
                for transactionTypeResponse in transactionTypeResponses {
                    if let data = transactionTypeResponse.data {
                        var transactionDataModels: [TransactionDataModel] = []
                        for dataResponse in data {
                            if let trxDate = dataResponse.trxDate, let nominal = dataResponse.nominal {
                                let dataModel = TransactionDataModel(trxDate: trxDate, nominal: nominal)
                                transactionDataModels.append(dataModel)
                            }
                        }
                        let transactionTypeModel = TransactionTypeModel(label: transactionTypeResponse.label,
                                                                        percentage: transactionTypeResponse.percentage,
                                                                        data: transactionDataModels)
                        transactionTypeModels.append(transactionTypeModel)
                    }
                }
            case .none:
                break
            }
            
            let portofolioModelElement = PortofolioModelElement(
                type: type,
                transaction: transactionTypeModels.isEmpty ? nil : transactionTypeModels,
                month: monthModel
            )
            
            portofolioModel.append(portofolioModelElement)
        }
        
        return portofolioModel
    }
    
    static func mapPromoResponseToModel(input promoResponse: PromoResponse) -> PromoModel {
        var promoModel = PromoModel()
        
        if let promosResponse = promoResponse.promos {
            var promoListModels: [PromoListModel] = []
            
            for promoResponse in promosResponse {
                let promoListModel = PromoListModel(
                    id: promoResponse.id,
                    name: promoResponse.name,
                    imagesURL: promoResponse.imagesURL,
                    detail: promoResponse.detail
                )
                
                promoListModels.append(promoListModel)
            }
            
            promoModel.promos = promoListModels
        }
        
        return promoModel
    }
    
    static func mapQrisModelToEntity(input qrisModel: QrisHistoryModel) -> QrisEntity {
        let qrisEntity = QrisEntity()
        qrisEntity.bank = qrisModel.bank ?? ""
        qrisEntity.transactionID = qrisModel.transactionID ?? ""
        qrisEntity.merchantName = qrisModel.merchantName ?? ""
        qrisEntity.amount = qrisModel.amount ?? 0
        qrisEntity.timeStamp = String(Date().timeIntervalSince1970)
        return qrisEntity
    }
    
    static func mapQrisEntityToModel(input qrisEntities: [QrisEntity]) -> [QrisHistoryModel] {
        var qrisHistoryModels: [QrisHistoryModel] = []
        
        for entity in qrisEntities {
            let qrisHistoryModel = QrisHistoryModel(
                bank: entity.bank,
                transactionID: entity.transactionID,
                merchantName: entity.merchantName,
                amount: entity.amount
            )
            qrisHistoryModels.append(qrisHistoryModel)
        }
        
        return qrisHistoryModels
    }
}
