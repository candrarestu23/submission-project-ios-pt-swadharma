//
//  TransactionAPI.swift
//  submissionProject
//
//  Created by candra restu on 05/03/24.
//

import Foundation

enum TransactionAPI {
    case getPromo
}

extension TransactionAPI: EndPointType {
    
    var urlRequest: URLRequest {
        switch self {
        case .getPromo:
            let fullURL = self.baseUrl.appendingPathComponent(self.path)
            var components =  URLComponents(string: fullURL.absoluteString)
            let tempComponent = components?.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            components?.percentEncodedQuery = tempComponent
            var request = URLRequest(url: (components?.url!)!)
            let bearerHeaderValue = "Bearer \(Constant.bearerToken)"
            request.setValue(bearerHeaderValue, forHTTPHeaderField: "Authorization")
            return request
        }
    }

    var baseUrl: URL {
        #if TEST
        return URL(string: "http://demo5853970.mockable.io")!
        #else
        return URL(string: "http://demo5853970.mockable.io")!
        #endif
        
    }

    var path: String {
        switch self {
        case .getPromo:
            return "/promos"
        }
    }

}
